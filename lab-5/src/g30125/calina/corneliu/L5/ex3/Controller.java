package g30125.calina.corneliu.L5.ex3;

public class Controller {

    TemperatureSensor ts;
    LightSensor ls;

    public Controller (TemperatureSensor ts, LightSensor ls){
        this.ts = ts;
        this.ls = ls;
    }

    public void control() throws InterruptedException {
        System.out.println(ts.readValue());
        Thread.sleep(1000);
        System.out.println(ls.readValue());
    }
}