package g30125.calina.corneliu.L5.ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor {

    public int readValue() {

        Random generator = new Random();
        return generator.nextInt(100);
    }
}
