package g30125.calina.corneliu.L5.ex1;

public class Square extends Rectangle {
    protected double side;
    public Square() {
        this.side = side;
    }
    public Square(double side) {
        this.side = side;
    }
    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);

    }
    public double getSide() {
        return side;
    }
    public void setSide(double side) {

        this.side = side;
        //super.length = side;
        //super.width = side;
    }
    @Override
    public void setWidth(double width) {
        this.getWidth();
    }
    public void setLength(double length){
        this.getLength();
    }
    public String toString(){
        return "A Square with side "+super.getWidth()+" which is a subclass of "+ super.toString();
    }
}
