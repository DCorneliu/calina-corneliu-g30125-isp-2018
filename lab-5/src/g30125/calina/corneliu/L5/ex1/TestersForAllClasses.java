package g30125.calina.corneliu.L5.ex1;

import java.util.*;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
public class TestersForAllClasses {
        @Test
        public void shouldreturnCirclePerimeter(){
            Circle circle3 = new Circle(2);
            assertEquals(12.56, circle3.getPerimeter(),001);
        }
        @Test
        public void shouldreturnCircleArea(){
            Circle circle4 = new Circle(2);
            assertEquals(12.56, circle4.getArea(), 001);
        }
        @Test
        public void shouldreturnRectanglePerimeter(){
            Rectangle rectangle4 = new Rectangle(3,2);
            assertEquals(10.0, rectangle4.getPerimeter(),001);
        }
        @Test
        public void shouldreturnRectangleArea(){
            Rectangle rectangle3 = new Rectangle(3,2);
            assertEquals(6.0, rectangle3.getArea(),001);
        }
    }

