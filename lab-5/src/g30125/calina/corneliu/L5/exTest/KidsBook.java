package g30125.calina.corneliu.L5.exTest;

public class KidsBook extends Book {
    public KidsBook(int nrOfPages)
    {
        super(nrOfPages);
    }

   @Override
    public String read(){
       return "Reading an KidsBook";
   }
}
