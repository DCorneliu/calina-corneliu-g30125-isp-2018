package g30125.calina.corneliu.L5.exTest;

public class ScienceFictionBook extends Book {
    public ScienceFictionBook(int nrOfPages)
    {
        super(nrOfPages);
    }
    @Override
    public String read(){
        return "Reading an ScienceFictionBook";
    }
}

