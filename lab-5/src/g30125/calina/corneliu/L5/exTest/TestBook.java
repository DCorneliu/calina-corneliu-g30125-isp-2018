package g30125.calina.corneliu.L5.exTest;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestBook{
    @Test
    public void shouldRead()
    {
        Book b=new Book(280);
        assertEquals("Reading a book", b.read());
    }

}
