package g30125.calina.corneliu.L5.exTest;

public class ActionBook extends Book {
    public ActionBook(int nrOfPages)
    {
        super(nrOfPages);
    }

    @Override
    public String read(){
        return "Reading an ActionBook";
    }

}
