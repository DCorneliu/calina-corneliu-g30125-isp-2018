package g30125.calina.corneliu.L5.ex2;

public interface Image {
    void display();
}

 class RealImage implements Image {

    private String fileName;

    public RealImage(String fileName){
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void display() {
        System.out.println("Displaying " + fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading " + fileName);
    }
}

 class ProxyImage implements Image {

    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private char type;

    public ProxyImage(String fileName, char type) {
        this.fileName = fileName;
        this.type = type;
    }

    @Override
    public void display() {
        if (type == 'r') {
            if (realImage == null) {
                realImage = new RealImage(fileName);
            }
            realImage.display();
        } else {

            if (rotatedImage == null) {
                rotatedImage = new RotatedImage(fileName);
            }
            rotatedImage.display();
        }
    }
    }


     class RotatedImage implements  Image{
        private String fileName;

        public RotatedImage(String fileName){
            this.fileName = fileName;
            loadFromDisk(fileName);
        }

        @Override
        public void display() {
            System.out.println("Display rotated " + fileName);
    }
        private void loadFromDisk(String fileName){
            System.out.println("Loading " + fileName);
        }
}