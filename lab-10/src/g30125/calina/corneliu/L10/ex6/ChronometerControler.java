package g30125.calina.corneliu.L10.ex6;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChronometerControler implements ActionListener {
    Chronometer c;
    GUI u;

    public ChronometerControler(Chronometer c, GUI u){
        this.c=c;
        this.u=u;
        u.getButton1().addActionListener(this);
        u.getButton2().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==u.getButton1()){
            c.changeState();
            u.getTextField().setText(c.k+"");
        } else if (e.getSource()==u.getButton2()){
            c.changeState();
            c.k=0;
        }
    }
}
