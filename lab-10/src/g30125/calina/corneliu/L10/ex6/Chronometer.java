package g30125.calina.corneliu.L10.ex6;


public class Chronometer implements Runnable{
    int k=0;
    Integer syncObj = 0;
    boolean p = true;

    Chronometer(){
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true){
            if(p){
                synchronized (syncObj){
                    try {
                        syncObj.wait();
                    } catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
            } else {
                synchronized (this){
                    this.notify();
                }
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
            k++;
        }
    }

    public void changeState() {
        p = !p;
        if(p){
            synchronized (syncObj){
                syncObj.notify();
            }
        }
    }
}
