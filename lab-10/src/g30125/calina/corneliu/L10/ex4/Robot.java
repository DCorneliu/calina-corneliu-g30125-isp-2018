package g30125.calina.corneliu.L10.ex4;


import java.util.Random;

public class Robot extends Thread{

    private final int[][] space;
    private int x;
    private int y;

    public Robot(int[][] space, int x,int y) {
        this.space = space;
        this.x = x;
        this.y = y;
    }

    @Override
    public void run() {
        int i = 0;
        Random r = new Random(4);
        while (i<5){
            int poz = r.nextInt();
            switch (poz){
                case 0: {
                    if (y>0)
                        y--;        //up
                    break;
                }
                case 1: {
                    if (x<10)       //right
                        x++;
                    break;
                }
                case 2: {
                    if (y<10)
                        y++;        //down
                    break;
                }
                case 4: {
                    if (x>0)
                        x--;        //left
                    break;
                }
            }
            space[x][y]++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (space[x][y] > 1){
                System.out.println("Destroy the robots.");
                space[x][y] = 0;
            }
            for (int k = 0; k<10; k++){
                for (int j = 0; j<10; j++){
                    System.out.print(space[k][j] + " ");
                }
                System.out.println();
            }
            System.out.println();
            i++;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
