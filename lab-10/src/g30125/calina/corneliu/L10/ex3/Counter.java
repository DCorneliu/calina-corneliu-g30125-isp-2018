package g30125.calina.corneliu.L10.ex3;

    public class Counter extends Thread {

        String n;
        Thread t;
        Counter(String n, Thread t){this.n = n;this.t=t;}

        public void run() {
            for (int i = 0; i < 100; i++) {
                try {
                    if (t==null)
                    System.out.println(getName() + " i = " + i);
                    else{
                        t.join();
                        System.out.println(getName() + " i = " + (i+100));}
                    Thread.sleep((int) (Math.random() * 100));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            System.out.println(getName() + " job finalised.");
        }

        public static void main(String[] args) {
            Counter c1 = new Counter("counter1",null);
            Counter c2 = new Counter("counter2",c1);

            c1.start();
            c2.start();

        }
    }

