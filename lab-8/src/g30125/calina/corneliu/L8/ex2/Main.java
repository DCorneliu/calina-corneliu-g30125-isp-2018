package g30125.calina.corneliu.L8.ex2;

import java.util.Scanner;

        public class Main {

            public static void main(String[] args) {

                FileReader newFile = new FileReader();
                System.out.print("Choose a caracter:");
                Scanner in = new Scanner(System.in);
                char caracter = in.next().charAt(0);
                in.close();
                System.out.println("The character appears " + newFile.countCharacter("data.txt",caracter) + " times.");
            }
        }
