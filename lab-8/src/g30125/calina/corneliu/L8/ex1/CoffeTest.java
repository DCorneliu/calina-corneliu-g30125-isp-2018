package g30125.calina.corneliu.L8;

import g30125.calina.corneliu.L8.ex1.NumberException;

public class CoffeTest {
    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();

        for(int i = 0;i<15;i++){
            Cofee c = mk.makeCofee();
            try {
                d.drinkCofee(c);
            } catch ( NumberException e ){
                System.out.println("Exception: "+e.getMessage()+" Coffees made: "+e.getNumber());}
            catch (TemperatureException e) {
                System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
            }

            finally{
                System.out.println("Throw the cofee cup.\n");
            }
        }
    }
}//.class

class CofeeMaker {
    int nr=0;
    Cofee makeCofee(){
        System.out.println("Make a coffe");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        nr++;
        Cofee cofee = new Cofee(t,c,nr);
        return cofee;
    }

}//.class

class Cofee{
    private int temp;
    private int conc;
    private int numar;

    Cofee(int t,int c, int nr){temp = t;conc = c; numar = nr;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    int getNr() {return numar;}
    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+"] ";}
}//.class

class CofeeDrinker{
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException, NumberException {
        if (c.getNr()>5)
            throw new NumberException(c.getNr(),"There are too many coffees!");
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");
        System.out.println("Drink cofee:"+c);
    }
}//.class

class TemperatureException extends Exception{
    int t;
    public TemperatureException(int t,String msg) {
        super(msg);
        this.t = t;
    }

    int getTemp(){
        return t;
    }
}//.class

class ConcentrationException extends Exception{
    int c;
    public ConcentrationException(int c,String msg) {
        super(msg);
        this.c = c;
    }

    int getConc(){
        return c;
    }


}//.class