package g30125.calina.corneliu.L8.ex1;

public class NumberException extends Exception{
    int number;
    public NumberException() {
    }

    public NumberException(int number, String message) {
        super(message);
        this.number=number;
    }

    public int getNumber() {
        return number;
    }
}