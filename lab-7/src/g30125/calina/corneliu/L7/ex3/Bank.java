package g30125.calina.corneliu.L7.ex3;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Bank
{
    TreeSet<BankAccount> Accounts = new TreeSet<>();
    public void addAccount(String owner, double balance)
    {
        Accounts.add(new BankAccount(owner,balance));
    }


    public void printAccounts()
    {
        Comparator<BankAccount> byBalance=Comparator.comparingDouble(BankAccount::getBalance);
        Supplier<TreeSet<BankAccount>> suplier=()->new TreeSet<BankAccount>(byBalance);

        TreeSet<BankAccount> sort = new TreeSet<>();
        sort=Accounts.stream().collect(Collectors.toCollection(suplier));
        for(BankAccount b: sort)
            System.out.println(b.getBalance()+" "+b.getOwner());
    }


    public void printAccounts(double minBalance, double maxBalance)
    {
        for(BankAccount b:Accounts)
            if(b.getBalance()>minBalance && b.getBalance()<maxBalance)
                System.out.println(b.getBalance()+" "+b.getOwner());
    }


    public TreeSet<BankAccount> getAllAccounts(){
        Comparator<BankAccount> byName=Comparator.comparing(BankAccount::getOwner);
        Supplier<TreeSet<BankAccount>> suplier=()->new TreeSet<BankAccount>(byName);

        return Accounts.stream().collect(Collectors.toCollection(suplier));

    }
    public static void main(String[] args) {
        Bank bank = new Bank();

        bank.addAccount("owner4",2435);
        bank.addAccount("owner5",2411);
        bank.addAccount("owner6",1242);
        bank.addAccount("owner7",4521);
        bank.addAccount("owner8",1750);
        bank.printAccounts();
        System.out.println(" ");
        bank.printAccounts(1500,3000);
        System.out.println(" ");
        for(BankAccount b:bank.getAllAccounts())
            System.out.println(b.getOwner()+" "+b.getBalance());
    }
}