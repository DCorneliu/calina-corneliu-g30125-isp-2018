package g30125.calina.corneliu.L7.ex2;

import java.util.ArrayList;
import java.util.Collections;


public class Bank {

    ArrayList<BankAccount> Conturi = new ArrayList<BankAccount>();
    public void addAccount (String owner, double balance){
        Conturi.add(new BankAccount(owner, balance));
    }
    public void printAccounts (){

        Collections.sort(Conturi);
        for (BankAccount b:Conturi)
            System.out.println(b.getBalance()+" "+b.getOwner());
    }
    public void printAccounts(double minBalance, double maxBalance){
        for (BankAccount b:Conturi)
            if (b.getBalance()>minBalance && b.getBalance()<maxBalance)
                System.out.println(b.getBalance()+" "+b.getOwner());
    }
    public BankAccount getAccount (String owner) {return null;}
    public ArrayList<BankAccount> getAllAccounts () {return Conturi;}

    public static void main(String[] args) {
        Bank bank = new Bank();

        bank.addAccount("owner4",2435);
        bank.addAccount("owner5",2411);
        bank.addAccount("owner6",1242);
        bank.addAccount("owner7",4521);
        bank.printAccounts();
        System.out.println(" ");
        bank.printAccounts(1500,3000);
        ArrayList<BankAccount> bankAccounts = bank.getAllAccounts();
        Collections.sort(bankAccounts,BankAccount.ownerComparator);
        System.out.println(" ");
        for(BankAccount b:bankAccounts)
            System.out.println(b.getOwner()+" "+b.getBalance());
    }

}
