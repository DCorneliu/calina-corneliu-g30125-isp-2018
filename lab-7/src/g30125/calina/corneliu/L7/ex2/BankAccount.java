package g30125.calina.corneliu.L7.ex2;

import java.util.Comparator;


public class BankAccount implements  Comparable<BankAccount> {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void withdraw(double amount) {
        this.balance -= amount;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    @Override
    public int hashCode() {
        return owner.hashCode() + (int) balance;
    }

    @Override
    public boolean equals(Object b) {
        if (b instanceof BankAccount) {
            BankAccount bank = (BankAccount) b;
            return (owner == ((BankAccount) b).owner && balance == ((BankAccount) b).balance);
        }
        return false;
    }
    public static Comparator<BankAccount> ownerComparator
            = new Comparator<BankAccount>() {

        public int compare(BankAccount b1, BankAccount b2) {

            return b1.getOwner().compareTo(b2.getOwner());

        }

    };

    @Override
    public int compareTo(BankAccount o) {
        return (int)(this.balance-o.getBalance());
    }
}
