package g30125.calina.corneliu.L7.ex4;


public class Word implements Comparable{

    private String name;

    public Word (String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }


    @Override
    public int compareTo(Object o) {
        String name = ((Word)o).getName();
        return this.name.compareTo(name);
    }

    @Override
    public boolean equals(Object obj) {
        Word o = (Word) obj;
        if (o!= null && this.name.equals(o.getName())){
            return true;
        } else
            return false;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result = result * name.hashCode();
        return result;
    }
}
