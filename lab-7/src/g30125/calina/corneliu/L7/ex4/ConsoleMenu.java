package g30125.calina.corneliu.L7.ex4;

import java.util.Scanner;

public class ConsoleMenu {

    public static void main(String[] args) {

        Dictionary d = new Dictionary();
        int option = 1;
        String word;
        String definition;

        while (option!=0){
            displayMenu();
            Scanner in = new Scanner(System.in);
            option = in.nextInt();

            switch (option) {
                case 1: {           //Adaugare cuvant
                    System.out.print("Introduceti cuvantul: ");
                    Scanner inWord = new Scanner(System.in);
                    word = inWord.next();
                    System.out.print("Introduceti definitia: ");
                    Scanner inDef = new Scanner(System.in);
                    definition = inDef.next();
                    d.addWord(new Word(word),new Definition(definition));
                    break;
                }
                case 2: {           //Afisare definitii pentru un cuvant dat
                    System.out.print("Give the word: ");
                    Scanner inWord = new Scanner(System.in);
                    word = inWord.next();
                    d.getDefinition(new Word(word));
                    break;
                }
                case 3: {          //Afisarea tuturor cuvintelor din dictionar
                    d.getAllWords();
                    break;
                }
                case 4: {           //Afisarea tuturor definitiilor din dictionar
                    d.getAllDefinitions();
                    break;
                }
                case 0: {           //iesire din meniu
                    break;
                }
            }
        }
    }

    private static void displayMenu() {
        System.out.println("Menu:");
        System.out.println("1. Adaugare cuvant");
        System.out.println("2. Afisare definitie");
        System.out.println("3. Afisare cuvinte");
        System.out.println("4. Afisare definitii");
        System.out.println("0. Iesire");
        System.out.println("Alegeti o optiune: ");
    }
}