package g30125.calina.corneliu.L7.ex4;

public class Definition {

    private String description;
    //private Word word;

    public Definition(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}