package g30125.calina.corneliu.L7.ex1;
import java.util.*;

public class BankAccount {
    private String owner;
    private double balance;
    BankAccount(String owner, double balance){
        this.owner = owner;
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void withdraw(double amount){

     }
    public void deposit(double amount){

    }

   @Override
    public boolean equals(Object obj) {
        if(obj instanceof BankAccount){
            BankAccount b = (BankAccount) obj;
            return b.owner.equalsIgnoreCase(owner)&&b.balance==balance;
        }
        else return false;
    }
    @Override
    public int hashCode() {

        return Objects.hash(owner, balance);
    }

     public static void main(String[] args){
         boolean test;
         //ArrayList<BankAccount> conturi = new ArrayList<>();
         BankAccount b1 = new BankAccount("owner1", 750);
         BankAccount b2 = new BankAccount("owner2", 1250);
         BankAccount b3 = new BankAccount("owner3", 1450);
        /* conturi.add(b1);
         conturi.add(b2);
         conturi.add(b3);
         for(int i=0;i<conturi.size();i++)
         {
             System.out.println(conturi.get(i).getOwner()+" "+conturi.get(i).getBalance());
         }*/
         test = b1.equals(b2);
         System.out.println(test);

    }
}
