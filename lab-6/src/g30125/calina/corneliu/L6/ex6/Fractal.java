package g30125.calina.corneliu.L6.ex6;

import javax.swing.*;
import java.awt.*;

public class Fractal extends JFrame {

    private int x;
    private int y;
    private int length;
    private int height;

    public Fractal(int x, int y, int length, int height) {
        this.x = x;
        this.y = y;
        this.length = length;
        this.height = height;
    }

    public void paint(Graphics g){
        while(length>20){
            g.setColor(Color.BLUE);
            g.fillOval(x,y,length,height);
            g.setColor(Color.GRAY);
            g.fillRect(x+6,y+6,length-6,length-6);
            x=(int)(x+(length-(Math.sqrt(length*length/2)))/2);
            y=(int)(y+(length-(Math.sqrt(length*length/2)))/2);
            length=(int)(Math.sqrt(length*length/2));
        }
    }


    public static void main(String[] args) {

        Fractal element = new Fractal(175,175,250,400);
        element.setTitle("Fractal");
        element.setSize(1000,1000);
        element.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        element.setVisible(true);
    }
}
