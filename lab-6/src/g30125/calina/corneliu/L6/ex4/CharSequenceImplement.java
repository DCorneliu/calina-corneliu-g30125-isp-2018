package g30125.calina.corneliu.L6.ex4;

public class CharSequenceImplement implements CharSequence{

    private char[] c;
    private int start;
    private int stop;

    public CharSequenceImplement(char[] chars) {
        this.c = chars;
    }

    public CharSequenceImplement(char[] chars, int start, int stop) {
        this.c = chars;
        this.start = start;
        this.stop = stop;
    }

    @Override
    public int length() {
        return c.length;
    }

    @Override
    public char charAt(int index) {
        return c[index];
    }

    @Override
    public CharSequence subSequence(int startSub, int stopSub) {
        start=0;
        stop=this.length();
        CharSequenceImplement subsequence = new CharSequenceImplement(c,start+startSub,stop-stopSub);
        return subsequence;
    }

    @Override
    public String toString() {
        return new String(this.c, this.start, this.stop);
    }
}
