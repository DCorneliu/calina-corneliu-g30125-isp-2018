package g30125.calina.corneliu.L6.ex1;

import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color, int x, int y, String id, boolean filled, int radius) {
        super(color,x,y,id,filled);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius, radius);
        if (isFilled()){
            g.fillOval(getX(),getY(),radius,radius);
        }
    }
}