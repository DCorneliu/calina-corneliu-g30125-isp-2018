package g30125.calina.corneliu.L6.ex1;
import java.awt.*;
public class Rectangle extends Shape{

      private int length;
    public Rectangle(Color color, int x, int y, String id, boolean filled, int length) {
        super(color,x,y,id,filled);
        this.length = length;
    }

    @Override
    public void draw(Graphics g){
        System.out.println("Drawing a rectangle "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(), getY(), length, length/2);
        if(isFilled())
            g.fillRect(getX(), getY(), length, length/2);
    }

}