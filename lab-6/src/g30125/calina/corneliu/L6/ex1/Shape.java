package g30125.calina.corneliu.L6.ex1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private int x, y;
    private String id;
    private boolean filled;

    public Shape(Color color, int x, int y, String id, boolean filled) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.filled = filled;
    }
    public String getId(){
        return this.id;
    }

    public boolean isFilled() {
        return this.filled;
    }

    public int getX() {
        return this.x;
    }
    public int getY() {
        return this.y;
    }
    public Color getColor() {
        return color;
    }
    public void setColor(Color color) {
        this.color = color;
    }
    public abstract void draw(Graphics g);
}
