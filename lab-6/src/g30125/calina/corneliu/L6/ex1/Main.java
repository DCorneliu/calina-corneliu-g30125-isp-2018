package g30125.calina.corneliu.L6.ex1;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,110,"abc",false,55);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.BLUE, 100, 150, "cerc",true,68);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.YELLOW, 74, 165, "rectangle",true,84);
        b1.addShape(s3);
    }
}

