package g30125.calina.corneliu.L6.ex3;

import javax.swing.*;
import java.awt.*;


public class DrawingBoard  extends JFrame {

    Shape[] shapes = new Shape[10];
    //ArrayList<Shape> shapes = new ArrayList<>();

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(500,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(Shape s1){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = s1;
                break;
            }
        }
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }
    }

    public void deleteByID(String id) {
        for (int i = 0; i<shapes.length; i++) {
            if (shapes[i]!=null && shapes[i].getId().equals(id)){
                shapes[i] = null;
            }
        }
        this.repaint();
    }
}