package g30125.calina.corneliu.L6.ex3;


import java.awt.*;

public interface Shape {
    public void draw(Graphics g);
    public String getId();
}
