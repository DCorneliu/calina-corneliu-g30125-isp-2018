package g30125.calina.corneliu.L6.ex3;

import java.awt.*;

public class Rectangle implements Shape{

    private Color color;
    private int x;
    private int y;
    private String id;
    private Boolean fill; //true if the shape should be drawn filled, false otherwise
    private int length;
    Graphics g;


    public Rectangle(Color color, int x, int y, String id, Boolean fill, int length) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.fill = fill;
        this.length = length;
    }

    public String getId() {
        return id;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void draw(Graphics g) {
        if (fill == true) {
            System.out.println("Drawing a filled rectangle " + length + " " + color.toString());
        } else
            System.out.println("Drawing a simple rectangle " + length + " " +color.toString());
        g.setColor(getColor());
        g.drawRect(x,y,length,length);
    }
}
