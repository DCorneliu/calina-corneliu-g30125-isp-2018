package g30125.calina.corneliu.L6.ex5;

import javax.swing.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Number of bricks:");
        int nr = in.nextInt();
        System.out.println("Width:");
        int width = in.nextInt();
        System.out.println("Height:");
        int height = in.nextInt();
        in.close();

        Brick brick = new Brick(height,width);
        Pyramid pyramid = new Pyramid(brick,nr);
        pyramid.buildPyramid();

        Pyramid_2 pyramid2 = new Pyramid_2(brick,nr);
        pyramid2.setTitle("Pyramid");
        pyramid2.setSize(1000,1000);
        pyramid2.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pyramid2.setVisible(true);

    }
}
