package g30125.calina.corneliu.L6.ex5;

import javax.swing.*;
import java.awt.*;

public class Pyramid_2 extends JFrame {

    Brick brick;
    int nr;


    public Pyramid_2(Brick brick, int nr) {
        this.brick = brick;
        this.nr = nr;
    }

    public void paint(Graphics g){
        super.paint(g);

        int currentX = nr*brick.getWidth()/2;
        int currentY = 80;
        int startX = currentX;

        int level = 1;
        while (nr > 0) {
            if ((level+1 <= nr-level) || (level == nr)) {
                for (int i=0; i<level; i++){
                    g.drawRect(currentX,currentY, brick.getWidth(), brick.getHeight());
                    currentX+= brick.getWidth();
                }
                nr-=level;
                level++;
                currentY+=brick.getHeight();
                currentX=startX-brick.getWidth()*(level-1)/2;
            }
            else {
                currentX=startX-brick.getWidth()*(nr-1)/2;
                for(int i=0; i<nr; i++){
                    g.drawRect(currentX,currentY, brick.getWidth(), brick.getHeight());
                    currentX+= brick.getWidth();
                }
                nr=0;
            }
        }
    }


}