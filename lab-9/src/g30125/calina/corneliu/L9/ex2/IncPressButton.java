package g30125.calina.corneliu.L9.ex2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class IncPressButton extends JFrame {

    private JPanel contentPane;
    private JTextField DisplayOne;

    int count;
    int countButton;


    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    IncPressButton frame = new IncPressButton();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public IncPressButton() {
        setTitle("Count button press");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);

        DisplayOne = new JTextField();
        panel.add(DisplayOne);
        DisplayOne.setColumns(10);


        JButton btnCountOne = new JButton("Count Button");
        countButton = 0;
        btnCountOne.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                DisplayOne.setText(Integer.toString(countButton++));
            }
        });
        panel.add(btnCountOne);

    }
}