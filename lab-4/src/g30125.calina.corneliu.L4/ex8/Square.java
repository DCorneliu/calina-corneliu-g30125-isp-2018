package g30125.calina.corneliu.L4.ex8;

public class Square extends Rectangle {
    private double side;
    public Square() {
        this.side = side;
    }
    public Square(double side) {
        this.side = side;
    }
    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);

    }
    public double getSide() {
        return side;
    }
    public void setSide(double side) {
        this.side = side;
    }
    @Override
    public void setWidth(double width) {
       this.getWidth();
    }
    public void setLength(double length){
        this.getLength();
    }
    public String toString(){
        return "A Square with side "+super.getWidth()+" which is a subclass of "+ super.toString();
    }
}
