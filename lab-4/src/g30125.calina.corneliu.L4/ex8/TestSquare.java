package g30125.calina.corneliu.L4.ex8;

public class TestSquare {
    public static void main(String[] args){
        Square square1=new Square(1.0);
        Square square2=new Square(4.0, "yellow", false);
        System.out.println(square1.toString());
        System.out.println(square2.toString());
    }
}