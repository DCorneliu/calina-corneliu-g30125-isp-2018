package g30125.calina.corneliu.L4.ex8;

public class Shape {
    private String color;
    private boolean filled;
    public Shape(){
     this.color = "green";
     this.filled = true;
    }
    public Shape(String color, boolean filled){
        this.color = color;
        this.filled = filled;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public boolean isFilled() {
        return filled;
    }
    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    public String toString()
    {
        if (isFilled() == true)
       return "A shape with color of "+getColor()+" and filled";
        else return "A shape with color of "+getColor()+" and Not filled";
    }
}
