package g30125.calina.corneliu.L4.ex2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestersForMyPoint {
    @Test
    public void shouldReturndistance()
    {
        MyPoint p = new MyPoint(3,1);
        assertEquals(5, p.distance(-1,-2), 0.01);
    }
}