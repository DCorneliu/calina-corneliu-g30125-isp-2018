package g30125.calina.corneliu.L4.ex9;

import becker.robots.*;

public class PoolDiving {
    public static void main(String[] args) {
        City California = new City();
        Wall blockAve0 = new Wall(California, 4, 3, Direction.EAST);
        Wall blockAve1 = new Wall(California, 3, 3, Direction.EAST);
        Wall blockAve2 = new Wall(California, 2, 3, Direction.EAST);
        Wall blockAve3 = new Wall(California, 4, 4, Direction.WEST);
        Wall blockAve4 = new Wall(California, 2, 4, Direction.NORTH);
        Wall blockAve5 = new Wall(California, 4, 4, Direction.SOUTH);
        Wall blockAve6 = new Wall(California, 4, 5, Direction.SOUTH);
        Wall blockAve7 = new Wall(California, 4, 5, Direction.EAST);
        Robot mark = new Robot(California, 1, 4, Direction.NORTH);
        mark.move();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.move();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.move();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.turnLeft();
        mark.move();
        mark.move();
        mark.move();
        mark.turnLeft();
        mark.turnLeft();
    }
}