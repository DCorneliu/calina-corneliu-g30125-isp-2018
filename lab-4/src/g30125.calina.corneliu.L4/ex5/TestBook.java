package g30125.calina.corneliu.L4.ex5;
import g30125.calina.corneliu.L4.ex4.Author;

public class TestBook {
    public static void main(String[] args){
        Book b1 = new Book("Luceafarul", new Author("Eminescu", "mihai.eminescu@junimea.ro",'M'), 15, 25 );
        Book b2 = new Book("The count of Monte Cristo", new Author("Alexandre Dumas", "alexandre.dumas@yahoo.fr",'M'), 30, 7);
        System.out.println(b1.toString());
        System.out.println(b2.toString());
    }
    // Testere cu assertequals
}
