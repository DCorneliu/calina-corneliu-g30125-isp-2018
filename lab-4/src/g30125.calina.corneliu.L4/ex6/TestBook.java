package g30125.calina.corneliu.L4.ex6;

import g30125.calina.corneliu.L4.ex4.Author;

public class TestBook {
    public static void main(String[] args)
    {
        Author[] authors = new Author[3];
        authors[0] = new Author("Slavici", "ioanslavici@yahoo.com", 'm');
        authors[1] = new Author("Ana Blandiana", "anablandiana@yahoo.com", 'f');
        authors[2] = new Author("Marin Preda", "marinpreda@yahoo.com", 'm');
        Book book1 = new Book("Nuvele", authors, 200, 20);
        System.out.println(book1.toString());
    }
}
