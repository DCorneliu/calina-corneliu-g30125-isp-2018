package g30125.calina.corneliu.L4.ex3;

import java.util.*;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestCircle {
    public static void main(String[] args) {
        Circle cerc1 = new Circle();
        Circle cerc2 = new Circle(2);
        System.out.println(cerc1);
        System.out.println(cerc2);
        System.out.println(cerc2.getRadius());
        System.out.println("Area: " + cerc2.getArea(2));
            assertEquals(12.566370614359172, cerc2.getArea(2), 0.01);
    }
}
