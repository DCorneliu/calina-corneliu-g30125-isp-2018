package g30125.calina.corneliu.L4.ex3;

import java.util.*;

public class Circle {
    private double radius;
    private String color;
     public Circle(){
          radius=1.0;
          color="red";
       }
       public Circle(double rad){
          this.radius=rad;
       }

    public double getRadius() {
        return radius;
    }

    public double getArea(double radius){
          return Math.PI*Math.pow(radius,2);
    }

}
