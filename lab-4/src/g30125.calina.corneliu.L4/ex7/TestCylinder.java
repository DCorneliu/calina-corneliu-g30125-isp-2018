package g30125.calina.corneliu.L4.ex7;

import java.util.*;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestCylinder {
    @Test
    public void shouldReturnArea()
    {
        Cylinder c = new Cylinder(2,1);
        assertEquals(37.69911184307751 , c.getArea(2),001);
    }
    @Test
    public void shouldReturnVolume()
    {
        Cylinder c2 = new Cylinder(3,2);
        assertEquals( 56.54866776461627, c2.getVolume(),001);
    }
}
