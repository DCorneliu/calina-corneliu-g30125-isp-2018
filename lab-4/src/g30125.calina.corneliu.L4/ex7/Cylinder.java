package g30125.calina.corneliu.L4.ex7;

import g30125.calina.corneliu.L4.ex3.Circle;

public class Cylinder extends Circle{
    private double height;
    public Cylinder(){
        double height = 1.0;
    }
    public Cylinder(double radius){
        super(radius);
    }
    public Cylinder(double radius, double height){
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }
    public double getVolume(){
        return Math.PI*Math.pow(getRadius(),2)*getHeight();
    }
    @Override
    public double getArea(double radius){
        return 2*Math.PI*radius*(radius+getHeight());
    }
}
