package g30125.calina.corneliu.L3.ex4;

import becker.robots.*;

public class ex4 //AroundTheWalls
    {
        public static void main(String[] args) {
            // Set up the initial situation
            City Wien = new City();
            Wall blockAve0 = new Wall(Wien, 1, 1, Direction.WEST);
            Wall blockAve1 = new Wall(Wien, 2, 1, Direction.WEST);
            Wall blockAve2 = new Wall(Wien, 1, 1, Direction.NORTH);
            Wall blockAve3 = new Wall(Wien, 1, 2, Direction.NORTH);
            Wall blockAve4 = new Wall(Wien, 2, 1, Direction.SOUTH);
            Wall blockAve5 = new Wall(Wien, 2, 2, Direction.SOUTH);
            Wall blockAve6 = new Wall(Wien, 2, 2, Direction.EAST);
            Wall blockAve7 = new Wall(Wien, 1, 2, Direction.EAST);
            Robot karel = new Robot(Wien, 0, 2, Direction.WEST);

            karel.move();
            karel.move();
            karel.turnLeft();
            karel.move();
            karel.move();
            karel.move();
            karel.turnLeft();
            karel.move();
            karel.move();
            karel.move();
            karel.turnLeft();
            karel.move();
            karel.move();
            karel.move();
            karel.turnLeft();
            karel.move();


        }
    }




