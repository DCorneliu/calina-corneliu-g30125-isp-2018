package g30125.calina.corneliu.L3.ex6;

public class MyPoint
{
    private int x,y;
    public MyPoint()
    {
        x=0;
        y=0;
    }

    public MyPoint(int x, int y)
    {
        this.x=x;
        this.y=y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    public void setX()
    {
        this.x=x;
    }

    public void setY(int y) {
        this.y = y;
    }
    public void setXY(int x, int y)
    {
        this.x=x;
        this.y = y;
    }
    String ToString()
    {
        return "("+this.x+","+this.y+")";
    }
    double distance(int x, int y)
    {
        return Math.sqrt(Math.pow(x-this.x,2)+Math.pow(y-this.y,2));
    }
    double distance(MyPoint another)
    {
        return Math.sqrt(Math.pow(another.getX()-this.x,2)+Math.pow(another.getY()-this.y,2));
    }
}