package g30125.calina.corneliu.L2;
import java.util.*;
public class ex7
{
    public static void main (String[] args)
    {
         Scanner input = new Scanner(System.in);
            int guestNumber,k=3,randomNumber;
            randomNumber=(int)(Math.random()*11);
            System.out.println("Guess a number between 0 and 10");
            while(k!=0)
            {
                System.out.print("Type your number: ");
                guestNumber=input.nextInt();
                if(guestNumber==randomNumber)
                {
                    System.out.println("You've guessed the number! Good job!");
                    break;
                }
                else if(guestNumber<randomNumber)
                {
                    System.out.println("The random number is bigger than " + guestNumber);
                    k--;
                }
                else if(guestNumber>randomNumber)
                {
                    System.out.println("The random number is less than " +guestNumber);
                    k--;
                }
            }
            if(k==0)
            {
                System.out.println("Game over!");
                System.out.println("The number was: "+randomNumber);
            }
    }
}
