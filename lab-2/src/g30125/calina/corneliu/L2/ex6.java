package g30125.calina.corneliu.L2;
import java.util.*;
/*
public class ex6 {
    public static void main(String[] args) {

        //non recursive method

        Scanner input = new Scanner(System.in);
        System.out.print("Enter number: ");
        int n = input.nextInt();
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        System.out.println("The factorial of " + n + " is " + result);
    }
}*/

//recursive method
        public class ex6
{

        static int factorial(int n) {
            if (n ==0)
                return 1;
            else
                return n * factorial(n-1);
        }
        public static void main(String[] args)
        {
            Scanner input = new Scanner(System.in);
            System.out.print("Enter the number: ");
            int n = input.nextInt();
            System.out.println("The factorial of "+n+" is " +factorial(n));
        }
}



